﻿namespace ASP.NETCoreWebAPI.DataTransferObjects
{
    public class DTOReadBook
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }


       
    }
}
