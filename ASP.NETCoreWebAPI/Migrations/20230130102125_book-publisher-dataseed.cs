﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ASP.NETCoreWebAPI.Migrations
{
    public partial class bookpublisherdataseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Publishers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Service = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publishers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: false),
                    Author = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ISBN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VolumeNo = table.Column<double>(type: "float", nullable: false),
                    BookUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseDate = table.Column<DateTime>(type: "Date", nullable: false),
                    PublisherId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_Publishers_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Publishers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Publishers",
                columns: new[] { "Id", "City", "Name", "Service" },
                values: new object[] { 1, "London", "Oxford printing", "Publishing" });

            migrationBuilder.InsertData(
                table: "Publishers",
                columns: new[] { "Id", "City", "Name", "Service" },
                values: new object[] { 2, "Oslo", "Tech publish printing", "Online Edition" });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "Author", "BookUrl", "Description", "ISBN", "Price", "PublisherId", "ReleaseDate", "Title", "VolumeNo" },
                values: new object[] { 1, "Saju Mohanan", "http://samplebook.com", "Learn the basis of C#", "BFD234334", 1500, 1, new DateTime(2022, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Fundamentals of C# programming", 1.1000000000000001 });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "Author", "BookUrl", "Description", "ISBN", "Price", "PublisherId", "ReleaseDate", "Title", "VolumeNo" },
                values: new object[] { 2, "Saju Mohanan", "http://samplebook.com", "Learn the basis of Python", "BFD234335", 1400, 1, new DateTime(2022, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Fundamentals of Python programming", 1.0 });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "Author", "BookUrl", "Description", "ISBN", "Price", "PublisherId", "ReleaseDate", "Title", "VolumeNo" },
                values: new object[] { 3, "Saju Mohanan", "http://samplebook.com", "Learn the basis of C++", "BFD234336", 1600, 1, new DateTime(2022, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Fundamentals of C++ programming", 1.2 });

            migrationBuilder.CreateIndex(
                name: "IX_Books_PublisherId",
                table: "Books",
                column: "PublisherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Publishers");
        }
    }
}
