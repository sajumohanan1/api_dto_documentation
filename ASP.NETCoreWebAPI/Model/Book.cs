﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCoreWebAPI.Model
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }
        public string BookUrl { get; set; }

        [Column(TypeName = "Date")] //2013-11-10 HH:MM:SS
        public DateTime ReleaseDate { get; set; }
        public Publisher Publisher { get; set; } //Navigation property

        //set the FK
        public int PublisherId { get; set; }
    }
}
