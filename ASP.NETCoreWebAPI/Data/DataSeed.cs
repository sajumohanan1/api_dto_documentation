﻿using ASP.NETCoreWebAPI.Model;
using System;
using System.Collections.Generic;
using static System.Reflection.Metadata.BlobBuilder;

namespace ASP.NETCoreWebAPI.Data
{
    public class DataSeed
    {
        public static List<Book> GetBooks()
        {
            List<Book> books = new List<Book>()
            {
                new Book()
                {
                    Id=1,
                    Title="Fundamentals of C# programming",
                    Price=1500,
                    Author="Saju Mohanan",
                    ISBN="BFD234334",
                    Description="Learn the basis of C#",
                    VolumeNo=1.1,
                    BookUrl="http://samplebook.com",
                    ReleaseDate=new DateTime(2022,10,10),
                    PublisherId=1
                },
                new Book()
                {
                    Id=2,
                    Title="Fundamentals of Python programming",
                    Price=1400,
                    Author="Saju Mohanan",
                    ISBN="BFD234335",
                    Description="Learn the basis of Python",
                    VolumeNo=1.0,
                    BookUrl="http://samplebook.com",
                    ReleaseDate=new DateTime(2022,10,10),
                    PublisherId=1
                },
                new Book()
                {
                    Id=3,
                    Title="Fundamentals of C++ programming",
                    Price=1600,
                    Author="Saju Mohanan",
                    ISBN="BFD234336",
                    Description="Learn the basis of C++",
                    VolumeNo=1.2,
                    BookUrl="http://samplebook.com",
                    ReleaseDate=new DateTime(2022,10,10),
                    PublisherId=1
                }
            };
            return books;   
        }

         public static List<Publisher> GetPublishers() 
        {
            List<Publisher> publishers = new List<Publisher>();
                publishers.Add(new Publisher()
                {
                    Id = 1,
                    Name = "Oxford printing",
                    Service = "Publishing",
                    City = "London"
                });

            publishers.Add(new Publisher()
            {
                Id = 2,
                Name = "Tech publish printing",
                Service = "Online Edition",
                City = "Oslo"
            });
            return publishers;

        }
       
    }
}
