﻿using ASP.NETCoreWebAPI.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCoreWebAPI.Data
{
    public class BooksDbContext : DbContext
    {
        public BooksDbContext(DbContextOptions options) : base(options)
        {
        }
        

        public DbSet<Book> Books { get; set; } //Physical table - Books
        public DbSet<Publisher> Publishers { get; set; } //Physical table - Books

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(DataSeed.GetBooks());
            modelBuilder.Entity<Publisher>().HasData(DataSeed.GetPublishers());
        }
    }
}
