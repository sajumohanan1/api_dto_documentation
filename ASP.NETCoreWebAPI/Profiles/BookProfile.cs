﻿using ASP.NETCoreWebAPI.DataTransferObjects;
using ASP.NETCoreWebAPI.Model;
using AutoMapper;

namespace ASP.NETCoreWebAPI.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, DTOReadBook>(); 
            CreateMap<DTOCreateBook, Book>(); 
        }

    }
}
