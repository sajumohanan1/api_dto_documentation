﻿using System.Collections.Generic;

namespace ASP.NETCoreWebAPI.Model
{
    public class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Service { get; set; }
        public string City { get; set; }

        //Nav property - Collection [To specify the relationship]
        public ICollection<Book> Books { get; set; }
    }
}