﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace ASP.NETCoreWebAPI.DataTransferObjects
{
    public class DTOCreateBook
    {      
        public string Title { get; set; }
        public int Price { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }          

     
        public int PublisherId { get; set; }
    }
}
